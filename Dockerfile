FROM quay.io/lordrob/quit-sensor-service-base:1.0.0
RUN pip install -U Flask
RUN dnf install wget -y
RUN pip install pms5003
RUN wget https://gitlab.com/iot-hackfest/sensor-adaptor/-/raw/master/server.py -O server.py
RUN dnf clean all
CMD ["python", "server.py"]