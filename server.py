from flask import Flask

from pms5003 import PMS5003, ReadTimeoutError

from enviroplus import gas


app = Flask(__name__)

pms5003 = PMS5003(
    device='/dev/ttyAMA0',
    baudrate=9600,
    pin_enable=22,
    pin_reset=27
)


@app.route('/getSerialNumber')
def getserial():
    # Extract serial from cpuinfo file
    cpuserial = "0000000000000000"
    try:
        f = open('/proc/cpuinfo', 'r')
        for line in f:
            if line[0:6] == 'Serial':
                cpuserial = line[10:26]
        f.close()
    except:
        cpuserial = "ERROR000000000"

    return cpuserial


@app.route('/getGas')
def airData():
    response = gas.read_all()
    print (response)
    return {
        "adc": response.adc,
        "reducing": response.reducing,
        "nh3": response.nh3,
        "oxidising": response.oxidising
    }


@app.route('/getPollution')
def gasData():
    response = pms5003.read()
    print(response)
    return {
        "PM1_0": response.data[0],
        "PM2_5": response.data[1],
        "PM10": response.data[2],
        "PM1_0_atm": response.data[3],
        "PM2_5_atm": response.data[4],
        "PM10_atm": response.data[5],
        "gt0_3um": response.data[6],
        "gt0_5um": response.data[7],
        "gt1_0um": response.data[8],
        "gt2_5um": response.data[9],
        "gt5_0um": response.data[10],
        "gt10um": response.data[11]
    }

if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0')
